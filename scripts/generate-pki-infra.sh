#!/bin/sh

# Initialize root CA.
ROOT_CA_DIR=pki/CA/root
cfssl gencert -initca $ROOT_CA_DIR/csr.json | cfssljson -bare $ROOT_CA_DIR/ca

# Initialize intermediate CA.
INTERMEDIATE_CA_DIR=pki/CA/intermediate
cfssl gencert \
	-initca $INTERMEDIATE_CA_DIR/csr.json | cfssljson -bare $INTERMEDIATE_CA_DIR/ca

# Sign intermediate CSR with root key.
cfssl sign \
	-ca $ROOT_CA_DIR/ca.pem \
	-ca-key $ROOT_CA_DIR/ca-key.pem \
	-config pki/config.json \
	-profile intermediate_ca \
	$INTERMEDIATE_CA_DIR/ca.csr | cfssljson -bare $INTERMEDIATE_CA_DIR/ca

# Create intermediate certificate chain.
cat $INTERMEDIATE_CA_DIR/ca.pem > $INTERMEDIATE_CA_DIR/bundle.pem
cat $ROOT_CA_DIR/ca.pem >> $INTERMEDIATE_CA_DIR/bundle.pem

# Initialize signing CA.
SIGNING_CA_DIR=pki/CA/signing
cfssl gencert \
	-initca $SIGNING_CA_DIR/csr.json | cfssljson -bare $SIGNING_CA_DIR/ca

# Sign signing CSR with intermediate key.
cfssl sign \
	-ca $INTERMEDIATE_CA_DIR/ca.pem \
	-ca-key $INTERMEDIATE_CA_DIR/ca-key.pem \
	-config pki/config.json \
	-profile signing_ca \
	$SIGNING_CA_DIR/ca.csr | cfssljson -bare $SIGNING_CA_DIR/ca

# Create signing certificate chain.
cat $SIGNING_CA_DIR/ca.pem > $SIGNING_CA_DIR/bundle.pem
cat $INTERMEDIATE_CA_DIR/bundle.pem >> $SIGNING_CA_DIR/bundle.pem

VPN_DIR=pki/servers/vpn
cfssl gencert \
	-ca $SIGNING_CA_DIR/ca.pem \
	-ca-key $SIGNING_CA_DIR/ca-key.pem \
	-config pki/config.json \
	-profile peer \
	$VPN_DIR/csr.json | cfssljson -bare $VPN_DIR/server

# Create vpn server certificate chain.
cat $VPN_DIR/server.pem > $VPN_DIR/bundle.pem
cat $SIGNING_CA_DIR/bundle.pem >> $VPN_DIR/bundle.pem

# Generate diffie helman parameters.
openssl dhparam -out $VPN_DIR/dh.pem 4096

VPN_CLIENT_DIR=pki/clients/vpn
cfssl gencert \
	-ca $SIGNING_CA_DIR/ca.pem \
	-ca-key $SIGNING_CA_DIR/ca-key.pem \
	-config pki/config.json \
	-profile peer \
	$VPN_CLIENT_DIR/csr.json | cfssljson -bare $VPN_CLIENT_DIR/client

# Create vpn client certificate chain.
cat $VPN_CLIENT_DIR/client.pem > $VPN_CLIENT_DIR/bundle.pem
cat $SIGNING_CA_DIR/bundle.pem >> $VPN_CLIENT_DIR/bundle.pem
