#!/bin/sh

find -name template.jsonnet -execdir sh -c 'jsonnet {} | yq read - >template.yaml' \;
