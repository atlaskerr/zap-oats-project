provider "aws" {
  region = "us-east-1"
}

resource "aws_cloudformation_stack" "instance_stack" {
  name          = "zap-instance-stack"
  template_body = file("template.yaml")
}
