provider "aws" {
  region = "us-east-1"
}

resource "aws_cloudformation_stack" "security_groups_stack" {
  name          = "zap-security-groups-stack"
  template_body = file("template.yaml")

  parameters = {
    "VpcId" = "vpc-0d7525f2106a50fb4"
  }
}
