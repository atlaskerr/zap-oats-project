provider "aws" {
  region = "us-east-1"
}

resource "aws_cloudformation_stack" "gateways_stack" {
  name          = "zap-gateways-stack"
  template_body = file("template.yaml")

  parameters = {
    "VpcId"           = "vpc-0d7525f2106a50fb4"
    "SubnetPublicAId" = "subnet-0a5be4340eec720fb"
    "SubnetPublicBId" = "subnet-06d004c34d2c380ca"
  }
}
