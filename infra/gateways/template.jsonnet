{
  AWSTemplateFormatVersion: '2010-09-09',
  Description: 'Gateways Template',
  Parameters: {

    VpcId: {
      Type: 'String',
      Description: 'VPC ID to add subnets to.',
    },
    SubnetPublicAId: {
      Type: 'String',
      Description: 'Subnet A ID.',
    },
    SubnetPublicBId: {
      Type: 'String',
      Description: 'Subnet B ID.',
    },

  },

  Resources: {

    // Internet Gateway
    InternetGateway: {
      name:: 'InternetGateway',
      Type: 'AWS::EC2::InternetGateway',
      Properties: {
        Tags: [{ Key: 'Name', Value: '' }],
      },
    },
    InternetGatewayAttachment: {
      Type: 'AWS::EC2::VPCGatewayAttachment',
      Properties: {
        InternetGatewayId: { Ref: 'InternetGateway' },
        VpcId: { Ref: 'VpcId' },
      },
    },

    // Elastic IPs for NAT gateways.
    NatAEip: { Type: 'AWS::EC2::EIP', Properties: { Domain: 'vpc' } },
    NatBEip: { Type: 'AWS::EC2::EIP', Properties: { Domain: 'vpc' } },

    // NAT Gateway A
    NatGatewayA: {
      Type: 'AWS::EC2::NatGateway',
      Properties: {
        AllocationId: { 'Fn::GetAtt': ['NatAEip', 'AllocationId'] },
        SubnetId: { Ref: 'SubnetPublicAId' },
      },
    },

    // NAT Gateway B
    NatGatewayB: {
      Type: 'AWS::EC2::NatGateway',
      Properties: {
        AllocationId: { 'Fn::GetAtt': ['NatBEip', 'AllocationId'] },
        SubnetId: { Ref: 'SubnetPublicBId' },
      },
    },

  },

  Outputs: {
    InternetGatewayId: {
      Description: 'Internet Gateway ID.',
      Value: { Ref: 'InternetGateway' },
    },
    NatGatewayAId: {
      Description: 'NAT Gateway A',
      Value: { Ref: 'NatGatewayA' },
    },
    NatGatewayBId: {
      Description: 'NAT Gateway B',
      Value: { Ref: 'NatGatewayB' },
    },
  },
}
