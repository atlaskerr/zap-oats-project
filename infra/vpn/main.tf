provider "aws" {
  region = "us-east-1"
}

resource "aws_cloudformation_stack" "vpn_stack" {
  name          = "zap-vpn-stack"
  template_body = file("template.yaml")
}
