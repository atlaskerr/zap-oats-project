{
  AWSTemplateFormatVersion: '2010-09-09',
  Description: 'VPC Template',
  Resources: {
    VPC: {
      Type: 'AWS::EC2::VPC',
      Properties: {
        CidrBlock: '10.55.0.0/16',
        EnableDnsSupport: true,
        EnableDnsHostnames: true,
        Tags: [{ Key: 'Name', Value: 'vpc-zap' }],
      },
    },
  },
  Outputs: {
    VpcId: {
      Description: 'VPC ID.',
      Value: { Ref: 'VPC' },
      Export: {
        Name: {
          'Fn::Sub': '${AWS::StackName}-VpcId',
        },
      },
    },
  },
}
