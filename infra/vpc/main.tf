provider "aws" {
  region = "us-east-1"
}

resource "aws_cloudformation_stack" "vpc_stack" {
  name          = "zap-vpc-stack"
  template_body = file("template.yaml")
}
