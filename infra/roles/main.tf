provider "aws" {
  region = "us-east-1"
}

resource "aws_cloudformation_stack" "roles_stack" {
  name          = "zap-roles-stack"
  capabilities  = ["CAPABILITY_NAMED_IAM"]
  template_body = file("template.yaml")
}
