local amazonLinux = 'ami-0ff8a91507f77f867';

{
  AWSTemplateFormatVersion: '2010-09-09',
  Description: '',
  Resources: {

    Instance: {
      Type: 'AWS::EC2::Instance',
      Properties: {
        InstanceType: 't3.micro',
        ImageId: amazonLinux,
        KeyName: 'admin',
        SubnetId: { 'Fn::ImportValue': 'ZapNetwork-SubnetPublicA' },
        SecurityGroupIds: [
          { 'Fn::ImportValue': 'ZapSecurityGroups-Bastion' },
        ],
      },
    },

  },
}
