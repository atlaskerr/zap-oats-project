local amazonLinux = 'ami-0ff8a91507f77f867';
local centos7 = 'ami-02eac2c0129f6376b';

{
  AWSTemplateFormatVersion: '2010-09-09',
  Description: '',
  Resources: {

    Instance: {
      Type: 'AWS::EC2::Instance',
      Properties: {
        InstanceType: 't3.micro',
        ImageId: centos7,
        KeyName: 'admin',
        SubnetId: { 'Fn::ImportValue': 'ZapNetwork-SubnetPublicA' },
        SecurityGroupIds: [
          { 'Fn::ImportValue': 'ZapSecurityGroups-VPN' },
        ],
      },
    },

    EIPAssociation: {
      Type: 'AWS::EC2::EIPAssociation',
      Properties: {
        AllocationId: { 'Fn::ImportValue': 'ZapNetwork-EIPVPNID' },
        InstanceId: { Ref: 'Instance' },
      },
    },

  },
}
