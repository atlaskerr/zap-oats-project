{
  AWSTemplateFormatVersion: '2010-09-09',
  Description: 'Playground Role',
  Resources: {

    Role: {
      Type: 'AWS::IAM::Role',
      Properties: {
        Description: 'IAM role for SSH bastion instances.',
        RoleName: 'playground-role',
        AssumeRolePolicyDocument: import 'assume-role.policy.json',
        ManagedPolicyArns: ['arn:aws:iam::aws:policy/ReadOnlyAccess'],
      },
    },

  },
}
