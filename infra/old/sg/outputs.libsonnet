{
  Bastion: {
    Value: { Ref: 'SecurityGroupBastion' },
    Export: {
      Name: { 'Fn::Sub': '${AWS::StackName}-Bastion' },
    },
  },
  VPN: {
    Value: { Ref: 'SecurityGroupVPN' },
    Export: {
      Name: { 'Fn::Sub': '${AWS::StackName}-VPN' },
    },
  },
  Playground: {
    Value: { Ref: 'SecurityGroupPlayground' },
    Export: {
      Name: { 'Fn::Sub': '${AWS::StackName}-Playground' },
    },
  },
}
