local bastion = import 'bastion.libsonnet';
local outputs = import 'outputs.libsonnet';
local playground = import 'playground.libsonnet';
local vpn = import 'vpn.libsonnet';

local newTemplate() = {
  AWSTemplateFormatVersion: '2010-09-09',
  Description: 'Zap Security Groups',
  Outputs: outputs,

  addResources(resources):: self {
    Resources+: {
      [resource.name]: resource
      for resource in resources
    },
  },
};

newTemplate()
.addResources(bastion)
.addResources(vpn)
.addResources(playground)
