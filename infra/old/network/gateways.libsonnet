[
  {
    name:: 'InternetGateway',
    Type: 'AWS::EC2::InternetGateway',
    Properties: {
      Tags: [
        {
          Key: 'Name',
          Value: '',
        },
      ],
    },
  },

  {
    name:: 'InternetGatewayAttachment',
    Type: 'AWS::EC2::VPCGatewayAttachment',
    Properties: {
      InternetGatewayId: {
        Ref: 'InternetGateway',
      },
      VpcId: {
        Ref: 'VPC',
      },
    },
  },

  {
    name:: 'NATGatewayOne',
    Type: 'AWS::EC2::NatGateway',
    Properties: {
      AllocationId: {
        'Fn::GetAtt': ['EIPNATOne', 'AllocationId'],
      }
      ,
      SubnetId: {
        Ref: 'SubnetPublicA',
      },
    },
  },

  {
    name:: 'NATGatewayTwo',
    Type: 'AWS::EC2::NatGateway',
    Properties: {
      AllocationId: {
        'Fn::GetAtt': ['EIPNATTwo', 'AllocationId'],
      }
      ,
      SubnetId: {
        Ref: 'SubnetPublicB',
      },
    },
  },

]
