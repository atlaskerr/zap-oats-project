local base = {
  Type: 'AWS::EC2::Subnet',
  Properties: {
    VpcId: { Ref: 'VPC' },
    AvailabilityZone: { 'Fn::Select': ['0', { 'Fn::GetAZs': '' }] },
  },
};

[
  base {
    name:: 'SubnetPublicA',
    Properties+: {
      CidrBlock: '10.55.0.0/28',
      MapPublicIpOnLaunch: true,
      Tags: [{ Key: 'Name', Value: 'subnet-one-public-us-east-1a' }],
    },
  },

  base {
    name:: 'SubnetPublicB',
    Properties+: {
      CidrBlock: '10.55.0.16/28',
      AvailabilityZone: { 'Fn::Select': ['1', { 'Fn::GetAZs': '' }] },
      MapPublicIpOnLaunch: true,
      Tags: [{ Key: 'Name', Value: 'subnet-two-public-us-east-1b' }],
    },
  },

  base {
    name:: 'SubnetPrivateA',
    Properties+: {
      CidrBlock: '10.55.0.32/28',
      Tags: [{ Key: 'Name', Value: 'subnet-one-private-us-east-1a' }],
    },
  },

  base {
    name:: 'SubnetPrivateB',
    Properties+: {
      CidrBlock: '10.55.0.48/28',
      AvailabilityZone: { 'Fn::Select': ['1', { 'Fn::GetAZs': '' }] },
      Tags: [{ Key: 'Name', Value: 'subnet-two-private-us-east-1b' }],
    },
  },
]
