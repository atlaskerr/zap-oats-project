local eips = import 'eips.libsonnet';
local gateways = import 'gateways.libsonnet';
local outputs = import 'outputs.libsonnet';
local routeTables = import 'route-tables.libsonnet';
local routes = import 'routes.libsonnet';
local subnets = import 'subnets.libsonnet';
local vpc = import 'vpc.libsonnet';

local newTemplate() = {
  AWSTemplateFormatVersion: '2010-09-09',
  Description: 'VPC network setup',
  Outputs: outputs,

  addResources(resources):: self {
    Resources+: {
      [resource.name]: resource
      for resource in resources
    },
  },
};

newTemplate()
.addResources(vpc)
.addResources(subnets)
.addResources(eips)
.addResources(routeTables)
.addResources(routes)
.addResources(gateways)
