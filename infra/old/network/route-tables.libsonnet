[

  {
    name:: 'RouteTablePublic',
    Type: 'AWS::EC2::RouteTable',
    Properties: {
      VpcId: { Ref: 'VPC' },
      Tags: [{ Key: 'Name', Value: 'rt-zap-public' }],
    },
  },

  {
    name:: 'RouteTablePrivateA',
    Type: 'AWS::EC2::RouteTable',
    Properties: {
      VpcId: { Ref: 'VPC' },
      Tags: [{ Key: 'Name', Value: 'rt-zap-private-us-east-1a' }],
    },
  },

  {
    name:: 'RouteTablePrivateB',
    Type: 'AWS::EC2::RouteTable',
    Properties: {
      VpcId: { Ref: 'VPC' },
      Tags: [{ Key: 'Name', Value: 'rt-zap-public-us-east-1b' }],
    },
  },

  {
    name:: 'RouteTableAssociationPublicSubnetOne',
    Type: 'AWS::EC2::SubnetRouteTableAssociation',
    Properties: {
      RouteTableId: { Ref: 'RouteTablePublic' },
      SubnetId: { Ref: 'SubnetPublicA' },
    },
  },

  {
    name:: 'RouteTableAssociationPublicSubnetTwo',
    Type: 'AWS::EC2::SubnetRouteTableAssociation',
    Properties: {
      RouteTableId: { Ref: 'RouteTablePublic' },
      SubnetId: { Ref: 'SubnetPublicB' },
    },
  },

  {
    name:: 'RouteTableAssociationPrivateSubnetOne',
    Type: 'AWS::EC2::SubnetRouteTableAssociation',
    Properties: {
      RouteTableId: { Ref: 'RouteTablePrivateA' },
      SubnetId: { Ref: 'SubnetPrivateA' },
    },
  },

  {
    name:: 'RouteTableAssociationPrivateSubnetTwo',
    Type: 'AWS::EC2::SubnetRouteTableAssociation',
    Properties: {
      RouteTableId: { Ref: 'RouteTablePrivateB' },
      SubnetId: { Ref: 'SubnetPrivateB' },
    },
  },
]
