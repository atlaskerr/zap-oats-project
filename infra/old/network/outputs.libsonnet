{
  VPCID: {
    Value: { Ref: 'VPC' },
    Export: {
      Name: { 'Fn::Sub': '${AWS::StackName}-VPCID' },
    },
  },
  SubnetPublicA: {
    Value: { Ref: 'SubnetPublicA' },
    Export: {
      Name: { 'Fn::Sub': '${AWS::StackName}-SubnetPublicA' },
    },
  },
  SubnetPrivateA: {
    Value: { Ref: 'SubnetPrivateA' },
    Export: {
      Name: { 'Fn::Sub': '${AWS::StackName}-SubnetPrivateA' },
    },
  },
  EIPVPN: {
    Value: {
      'Fn::GetAtt': [
        'EIPVPN',
        'AllocationId',
      ],
    },
    Export: {
      Name: { 'Fn::Sub': '${AWS::StackName}-EIPVPNID' },
    },
  },
}
