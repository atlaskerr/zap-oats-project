[
  {
    name:: 'VPC',
    Type: 'AWS::EC2::VPC',
    Properties: {
      CidrBlock: '10.55.0.0/16',
      EnableDnsSupport: true,
      EnableDnsHostnames: true,
      Tags: [{ Key: 'Name', Value: 'vpc-zap' }],
    },
  },
]
