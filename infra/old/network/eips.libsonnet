[
  {
    name:: 'EIPNATOne',
    Type: 'AWS::EC2::EIP',
    Properties: {
      Domain: 'vpc',
    },
  },
  {
    name:: 'EIPNATTwo',
    Type: 'AWS::EC2::EIP',
    Properties: {
      Domain: 'vpc',
    },
  },
  {
    name:: 'EIPVPN',
    Type: 'AWS::EC2::EIP',
    Properties: {
      Domain: 'vpc',
    },
  },
]
