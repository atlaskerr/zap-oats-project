[
  {
    name:: 'RouteInternetGateway',
    Type: 'AWS::EC2::Route',
    Properties: {
      DestinationCidrBlock: '0.0.0.0/0',
      GatewayId: {
        Ref: 'InternetGateway',
      },
      RouteTableId: {
        Ref: 'RouteTablePublic',
      },
    },
  },

  {
    name:: 'RouteNATGatewayOne',
    Type: 'AWS::EC2::Route',
    Properties: {
      DestinationCidrBlock: '0.0.0.0/0',
      RouteTableId: {
        Ref: 'RouteTablePrivateA',
      },
      NatGatewayId: {
        Ref: 'NATGatewayOne',
      },
    },
  },

  {
    name:: 'RouteNATGatewayTwo',
    Type: 'AWS::EC2::Route',
    Properties: {
      DestinationCidrBlock: '0.0.0.0/0',
      RouteTableId: {
        Ref: 'RouteTablePrivateB',
      },
      NatGatewayId: {
        Ref: 'NATGatewayTwo',
      },
    },
  },
]
