provider "aws" {
  region = "us-east-1"
}

resource "aws_cloudformation_stack" "bastion_stack" {
  name          = "zap-bastion-stack"
  template_body = file("template.yaml")

  parameters = {
    "SecurityGroupId" = "sg-0b50b30253b5bce5e"
    "SubnetId"        = "subnet-0a5be4340eec720fb"
  }
}
