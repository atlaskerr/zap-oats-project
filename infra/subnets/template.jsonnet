{
  AWSTemplateFormatVersion: '2010-09-09',
  Description: 'Subnets Template',
  Parameters: {
    VpcId: {
      Type: 'String',
      Description: 'VPC ID to add subnets to.',
    },
  },
  Resources: {

    // Public Subnet A
    PublicA: {
      Type: 'AWS::EC2::Subnet',
      Properties: {
        VpcId: { Ref: 'VpcId' },
        CidrBlock: '10.55.0.0/28',
        MapPublicIpOnLaunch: true,
        AvailabilityZone: { 'Fn::Select': ['0', { 'Fn::GetAZs': '' }] },
        Tags: [{ Key: 'Name', Value: 'subnet-one-public-us-east-1a' }],
      },
    },

    // Public Subnet B
    PublicB: {
      Type: 'AWS::EC2::Subnet',
      Properties: {
        VpcId: { Ref: 'VpcId' },
        CidrBlock: '10.55.0.16/28',
        AvailabilityZone: { 'Fn::Select': ['1', { 'Fn::GetAZs': '' }] },
        MapPublicIpOnLaunch: true,
        Tags: [{ Key: 'Name', Value: 'subnet-two-public-us-east-1b' }],
      },
    },

    // Private Subnet A
    PrivateA: {
      Type: 'AWS::EC2::Subnet',
      Properties: {
        VpcId: { Ref: 'VpcId' },
        AvailabilityZone: { 'Fn::Select': ['0', { 'Fn::GetAZs': '' }] },
        CidrBlock: '10.55.0.32/28',
        Tags: [{ Key: 'Name', Value: 'subnet-one-private-us-east-1a' }],
      },
    },

    // Private Subnet B
    PrivateB: {
      Type: 'AWS::EC2::Subnet',
      Properties: {
        VpcId: { Ref: 'VpcId' },
        CidrBlock: '10.55.0.48/28',
        AvailabilityZone: { 'Fn::Select': ['1', { 'Fn::GetAZs': '' }] },
        Tags: [{ Key: 'Name', Value: 'subnet-two-private-us-east-1b' }],
      },
    },

  },
  Outputs: {
    PublicAId: {
      Value: { Ref: 'PublicA' },
      Export: { Name: { 'Fn::Sub': '${AWS::StackName}-PublicAId' } },
    },
    PublicBId: {
      Value: { Ref: 'PublicB' },
      Export: { Name: { 'Fn::Sub': '${AWS::StackName}-PublicBId' } },
    },
    PrivateAId: {
      Value: { Ref: 'PrivateA' },
      Export: { Name: { 'Fn::Sub': '${AWS::StackName}-PrivateAId' } },
    },
    PrivateBId: {
      Value: { Ref: 'PrivateB' },
      Export: { Name: { 'Fn::Sub': '${AWS::StackName}-PrivateBId' } },
    },
  },
}
