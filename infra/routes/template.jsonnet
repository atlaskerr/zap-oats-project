{
  AWSTemplateFormatVersion: '2010-09-09',
  Description: 'Routes Template',
  Parameters: {
    VpcId: {
      Type: 'String',
      Description: 'VPC ID to add tables and routes to.',
    },
    InternetGatewayId: {
      Type: 'String',
      Description: 'ID of the internet gateway to route traffic out of VPC.',
    },
    NatGatewayAId: {
      Type: 'String',
      Description: 'ID of the NAT gateway to route traffic out of the VPC',
    },
    NatGatewayBId: {
      Type: 'String',
      Description: 'ID of the NAT gateway to route traffic out of the VPC',
    },
    SubnetPublicAId: {
      Type: 'String',
      Description: 'ID for a public subnet in a unique availability zone.',
    },
    SubnetPublicBId: {
      Type: 'String',
      Description: 'ID for a public subnet in a unique availability zone.',
    },
    SubnetPrivateAId: {
      Type: 'String',
      Description: 'ID for a private subnet in a unique availability zone.',
    },
    SubnetPrivateBId: {
      Type: 'String',
      Description: 'ID for a private subnet in a unique availability zone.',
    },
  },
  Resources: {

    // Route tables.
    RouteTablePublic: {
      Type: 'AWS::EC2::RouteTable',
      Properties: {
        VpcId: { Ref: 'VpcId' },
        Tags: [{ Key: 'Name', Value: 'rt-zap-public' }],
      },
    },
    RouteTablePrivateA: {
      Type: 'AWS::EC2::RouteTable',
      Properties: {
        VpcId: { Ref: 'VpcId' },
        Tags: [{ Key: 'Name', Value: 'rt-zap-private-a' }],
      },
    },
    RouteTablePrivateB: {
      Type: 'AWS::EC2::RouteTable',
      Properties: {
        VpcId: { Ref: 'VpcId' },
        Tags: [{ Key: 'Name', Value: 'rt-zap-private-b' }],
      },
    },

    // Route table and subnet associations.
    AssociationPublicSubnetPublicA: {
      Type: 'AWS::EC2::SubnetRouteTableAssociation',
      Properties: {
        RouteTableId: { Ref: 'RouteTablePublic' },
        SubnetId: { Ref: 'SubnetPublicAId' },
      },
    },
    AssociationPublicSubnetPublicB: {
      Type: 'AWS::EC2::SubnetRouteTableAssociation',
      Properties: {
        RouteTableId: { Ref: 'RouteTablePublic' },
        SubnetId: { Ref: 'SubnetPublicBId' },
      },
    },
    AssociationPrivateSubnetPrivateA: {
      Type: 'AWS::EC2::SubnetRouteTableAssociation',
      Properties: {
        RouteTableId: { Ref: 'RouteTablePrivateA' },
        SubnetId: { Ref: 'SubnetPrivateAId' },
      },
    },
    AssociationPrivateSubnetPrivateB: {
      Type: 'AWS::EC2::SubnetRouteTableAssociation',
      Properties: {
        RouteTableId: { Ref: 'RouteTablePrivateB' },
        SubnetId: { Ref: 'SubnetPrivateBId' },
      },
    },

    // Routes
    RouteInternetGateway: {
      Type: 'AWS::EC2::Route',
      Properties: {
        DestinationCidrBlock: '0.0.0.0/0',
        GatewayId: { Ref: 'InternetGatewayId' },
        RouteTableId: { Ref: 'RouteTablePublic' },
      },
    },
    RouteNatGatewayA: {
      Type: 'AWS::EC2::Route',
      Properties: {
        DestinationCidrBlock: '0.0.0.0/0',
        RouteTableId: { Ref: 'RouteTablePrivateA' },
        NatGatewayId: { Ref: 'NatGatewayAId' },
      },
    },
    RouteNatGatewayB: {
      Type: 'AWS::EC2::Route',
      Properties: {
        DestinationCidrBlock: '0.0.0.0/0',
        RouteTableId: { Ref: 'RouteTablePrivateB' },
        NatGatewayId: { Ref: 'NatGatewayBId' },
      },
    },

  },
}
