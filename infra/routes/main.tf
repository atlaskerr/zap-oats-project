provider "aws" {
  region = "us-east-1"
}

resource "aws_cloudformation_stack" "routes_stack" {
  name          = "zap-routes-stack"
  template_body = file("template.yaml")

  parameters = {
    "VpcId"             = "vpc-0d7525f2106a50fb4"
    "InternetGatewayId" = "igw-08d59aadd30a0fe59"
    "NatGatewayAId"     = "nat-0c53524528c35f288"
    "NatGatewayBId"     = "nat-0a858ddf849f4544e"
    "SubnetPublicAId"   = "subnet-0a5be4340eec720fb"
    "SubnetPublicBId"   = "subnet-06d004c34d2c380ca"
    "SubnetPrivateAId"  = "subnet-06842dcf1fe57b89e"
    "SubnetPrivateBId"  = "subnet-0a0393dd02a61713a"
  }
}
