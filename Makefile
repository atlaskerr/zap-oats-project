.PHONY: gen
gen:
	scripts/generate-templates.sh

.PHONY: pki
pki:
	scripts/generate-pki-infra.sh
